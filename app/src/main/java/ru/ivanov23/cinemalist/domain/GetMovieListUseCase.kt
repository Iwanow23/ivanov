package ru.ivanov23.cinemalist.domain

import androidx.paging.PagingData
import kotlinx.coroutines.flow.Flow
import ru.ivanov23.cinemalist.data.MovieRepository
import ru.ivanov23.cinemalist.data.model.Movie
import ru.ivanov23.cinemalist.data.model.MovieListTypes
import javax.inject.Inject

/**
 * UseCase для получения списка фильмов в зависимости от типа.
 *
 * @param repository Репозиторий для доступа к данным фильмов.
 */
class GetMovieListUseCase @Inject constructor(
    private val repository: MovieRepository
) {

    /**
     * Функция: получение списка фильмов в зависимости от типа и запроса.
     *
     * @param type Тип списка фильмов (POPULAR - популярные, FAVORITE - избранные).
     * @param query Поисковый запрос для фильтрации списка (может быть пустым).
     * @return Поток данных с постраничной информацией о фильмах.
     */
    operator fun invoke(type: MovieListTypes, query: String): Flow<PagingData<Movie>> =
        when(type) {
            MovieListTypes.POPULAR -> repository.getMovieListFromApi(query) // Получение списка фильмов из сети
            MovieListTypes.FAVORITE -> repository.getMovieListFromDb(query) // Получение списка избранных фильмов из базы данных
        }
}