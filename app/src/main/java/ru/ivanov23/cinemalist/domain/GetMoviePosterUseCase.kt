package ru.ivanov23.cinemalist.domain

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import ru.ivanov23.cinemalist.data.model.PosterTypes
import ru.ivanov23.cinemalist.data.network.MovieApiService
import javax.inject.Inject

/**
 * Класс, представляющий UseCase (слой бизнес-логики) для получения постера фильма по идентификатору.
 *
 * @param apiService Сервис API для получения данных о фильмах.
 */
class GetMoviePosterUseCase @Inject constructor(
    private val apiService: MovieApiService
) {

    /**
     * Функция для выполнения UseCase: получение постера фильма по идентификатору.
     *
     * @param type Тип запроса ("pr" - обычное изображение, "kp" - превью постера).
     * @param filmId Идентификатор фильма.
     * @return Объект Bitmap с изображением постера или превью.
     */
    suspend operator fun invoke(type: PosterTypes, filmId: Long): Bitmap {
        val img = when(type) {
            PosterTypes.PREVIEW -> apiService.getMoviePosterPreview(filmId).bytes() // Получение байтов изображения превью постера
            PosterTypes.FULL -> apiService.getMoviePoster(filmId).bytes() // Получение байтов изображения постера по полному размеру
        }

        return BitmapFactory.decodeByteArray(img, 0, img.size) // Декодирование массива байтов в объект Bitmap
    }
}
