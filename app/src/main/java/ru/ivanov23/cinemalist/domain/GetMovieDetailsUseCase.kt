package ru.ivanov23.cinemalist.domain

import ru.ivanov23.cinemalist.data.network.MovieApiService
import ru.ivanov23.cinemalist.data.model.MovieDetails
import ru.ivanov23.cinemalist.data.network.model.RestMovieDetailsResponse
import javax.inject.Inject

/**
 * UseCase для получения подробной информации о фильме.
 *
 * @param apiService Сервис для взаимодействия с сетью.
 */
class GetMovieDetailsUseCase @Inject constructor(
    private val apiService: MovieApiService
) {

    /**
     * Функция: получение подробной информации о фильме.
     *
     * @param filmId ID фильма, информацию о котором необходимо получить.
     * @return Подробная информация о фильме.
     */
    suspend operator fun invoke(filmId: Long): MovieDetails =
        apiService.getMovieDetails(filmId).toMovieDetails() // Преобразование данных из сети в объект MovieDetails
}

private fun RestMovieDetailsResponse.toMovieDetails(): MovieDetails =
    MovieDetails(
        filmId = filmId,
        nameRu = nameRu,
        year = year,
        genres = genres.flatMap { it.entries }
            .joinToString(", ") { (key, value) -> value },
        description = description,
        countries = countries.flatMap { it.entries }
            .joinToString(", ") { (key, value) -> value },
        posterUrl = posterUrl,
        poster = null
    )
