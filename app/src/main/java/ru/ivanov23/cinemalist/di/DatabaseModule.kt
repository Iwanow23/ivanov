package ru.ivanov23.cinemalist.di

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import ru.ivanov23.cinemalist.data.database.AppDatabase
import ru.ivanov23.cinemalist.data.database.MovieDao
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class DatabaseModule {

    @Provides
    @Singleton
    fun provideDatabase(@ApplicationContext context: Context): AppDatabase {
        return Room.databaseBuilder(
            context.applicationContext,
            AppDatabase::class.java,
            "appDatabase"
        ).build()
    }

    @Provides
    @Singleton
    fun provideFavoriteMovieDao(appDatabase: AppDatabase): MovieDao {
        return appDatabase.favoriteMovieDao()
    }
}