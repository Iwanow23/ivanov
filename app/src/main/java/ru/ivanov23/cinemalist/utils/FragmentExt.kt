package ru.ivanov23.cinemalist.utils

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment

/**
 * Функция: устанавливает заголовок в ActionBar для фрагмента.
 *
 * @param text Текст заголовка, который необходимо установить.
 */
fun Fragment.setTitle(text: String?) {
    (activity as? AppCompatActivity)?.supportActionBar?.title = text
}