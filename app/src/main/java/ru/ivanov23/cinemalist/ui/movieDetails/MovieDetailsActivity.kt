package ru.ivanov23.cinemalist.ui.movieDetails

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import by.kirich1409.viewbindingdelegate.viewBinding
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import ru.ivanov23.cinemalist.R
import ru.ivanov23.cinemalist.data.model.MovieDetails
import ru.ivanov23.cinemalist.databinding.ActivityMovieDetailsBinding
import ru.ivanov23.cinemalist.databinding.LayoutErrorViewBinding

@AndroidEntryPoint
class MovieDetailsActivity : AppCompatActivity(R.layout.activity_movie_details) {

    private val viewBinding: ActivityMovieDetailsBinding by viewBinding(ActivityMovieDetailsBinding::bind)
    private val viewModel: MovieDetailsViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setSupportActionBar(viewBinding.toolbar)

        setupActionBar()
        setupErrorView()

        observeViewModel()
    }

    private fun setupActionBar() {
        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            title = ""
        }
    }

    private fun setupErrorView() {
        viewBinding.errorView.setOnInflateListener { _, inflated ->
            LayoutErrorViewBinding.bind(inflated).apply {
                errorActionButton.setOnClickListener {
                    viewModel.loadData()
                }
            }
        }
    }

    private fun observeViewModel() {
        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.viewState.collectLatest(::showNewState)
            }
        }
    }

    private fun showNewState(state: MovieDetailsViewState) {
        when(state) {
            MovieDetailsViewState.Loading -> with(viewBinding) {
                contentScrollView.visibility = View.INVISIBLE
                collapsingBarLayout?.visibility = View.INVISIBLE
                errorView.isVisible = false
                loaderView.show()
            }
            MovieDetailsViewState.Error -> with(viewBinding) {
                contentScrollView.visibility = View.INVISIBLE
                collapsingBarLayout?.visibility = View.INVISIBLE
                errorView.isVisible = true
                loaderView.hide()
            }
            is MovieDetailsViewState.Success -> with(viewBinding) {
                showMovieDetails(state.data)
                contentScrollView.visibility = View.VISIBLE
                collapsingBarLayout?.visibility = View.VISIBLE
                errorView.isVisible = false
                loaderView.hide()
            }
        }
    }

    private fun showMovieDetails(viewData: MovieDetails) {
        with(viewBinding) {
            Glide.with(this@MovieDetailsActivity)
                .load(viewData.poster)
                .transition(DrawableTransitionOptions.withCrossFade())
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)
                .into(moviePoster)

            movieTitle?.text = viewData.nameRu
            movieDescription.text = viewData.description
            movieGenres.text = viewData.genres
            movieCountries.text = viewData.countries

            supportActionBar?.title = viewData.nameRu
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressedDispatcher.onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}