package ru.ivanov23.cinemalist.ui.movieList

import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import ru.ivanov23.cinemalist.R
import ru.ivanov23.cinemalist.data.model.Movie
import ru.ivanov23.cinemalist.databinding.ItemMovieBinding

class MovieViewHolder(
    viewBinding: ItemMovieBinding,
    private val onMovieShortClick: (filmId: Long) -> Unit,
    private val onMovieLongClick: (film: Movie) -> Boolean,
) : RecyclerView.ViewHolder(viewBinding.root) {

    private val rootView = viewBinding.root
    private val imageView = viewBinding.moviePoster
    private val titleView = viewBinding.movieTitle
    private val genreView = viewBinding.movieGenreAndYear
    private val favoriteView = viewBinding.favoriteIcon

    /**
     * Функция: устанавливает данные фильма во ViewHolder.
     *
     * @param data объект заказа для отображения
     */
    fun setData(data: Movie) {
        Glide.with(imageView)
            .load(data.posterUrl)
            .placeholder(R.drawable.no_poster)
            .transform(RoundedCorners(24))
            .into(imageView)
        titleView.text = data.nameRu
        genreView.text =  String.format(R.string.genres_year.getString(), data.genres, data.year)
        favoriteView.isVisible = data.isFavorite
        rootView.setOnClickListener { onMovieShortClick(data.filmId) }
        rootView.setOnLongClickListener { onMovieLongClick(data) }
    }

    /**
     * Расширение для получения строки из ресурсов по заданному идентификатору.
     *
     * @return строковое значение ресурса
     */
    private fun Int.getString(): String =
        rootView.context.getString(this)
}