package ru.ivanov23.cinemalist.ui.movieDetails

import ru.ivanov23.cinemalist.data.model.MovieDetails

/**
 * Представляет состояние экрана деталей фильма.
 */
sealed class MovieDetailsViewState {

    /**
     * Состояние загрузки данных.
     */
    data object Loading : MovieDetailsViewState()

    /**
     * Состояние ошибки.
     */
    data object Error : MovieDetailsViewState()

    /**
     * Состояние успешной загрузки данных.
     * @param data данные о фильме
     */
    data class Success(val data: MovieDetails) : MovieDetailsViewState()
}