package ru.ivanov23.cinemalist.ui.movieList

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import ru.ivanov23.cinemalist.data.model.Movie
import ru.ivanov23.cinemalist.databinding.ItemMovieBinding

class MoviePagingAdapter (
    private val onMovieShortClick: (filmId: Long) -> Unit,
    private val onMovieLongClick: (film: Movie) -> Boolean,
) : PagingDataAdapter<Movie, MovieViewHolder>(DiffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = ItemMovieBinding.inflate(layoutInflater, parent, false)
        return MovieViewHolder(binding, onMovieShortClick, onMovieLongClick)
    }

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        holder.setData(getItem(position) as Movie)
    }
}

private object DiffCallback : DiffUtil.ItemCallback<Movie>() {
    override fun areItemsTheSame(oldItem: Movie, newItem: Movie): Boolean =
        oldItem.filmId == newItem.filmId

    override fun areContentsTheSame(oldItem: Movie, newItem: Movie): Boolean =
        oldItem == newItem
}