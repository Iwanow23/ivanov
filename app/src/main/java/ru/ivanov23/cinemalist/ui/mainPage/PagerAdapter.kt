package ru.ivanov23.cinemalist.ui.mainPage

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import ru.ivanov23.cinemalist.data.model.MovieListTypes
import ru.ivanov23.cinemalist.ui.movieList.MovieListFragment

class PagerAdapter(fragment: Fragment) : FragmentStateAdapter(fragment) {

    override fun getItemCount(): Int = NUM_PAGES

    override fun createFragment(position: Int): Fragment {
        return when (position) {
            0 -> createMovieListFragment(MovieListTypes.POPULAR)
            else -> createMovieListFragment(MovieListTypes.FAVORITE)
        }
    }

    private fun createMovieListFragment(movieListType: MovieListTypes): Fragment {
        return MovieListFragment.getInstance(movieListType)
    }

    companion object {
        private const val NUM_PAGES = 2
    }
}