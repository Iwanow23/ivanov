package ru.ivanov23.cinemalist.ui.movieList

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import androidx.paging.map
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch
import ru.ivanov23.cinemalist.data.MovieRepository
import ru.ivanov23.cinemalist.domain.GetMovieListUseCase
import ru.ivanov23.cinemalist.data.model.Movie
import ru.ivanov23.cinemalist.data.model.MovieListTypes
import ru.ivanov23.cinemalist.ui.movieList.MovieListFragment.Companion.MOVIE_LIST_TYPE_ARG_KEY
import javax.inject.Inject

@HiltViewModel
class MovieListViewModel @Inject constructor(
    private val movieListUseCase: GetMovieListUseCase,
    private val movieRepository: MovieRepository,
    savedStateHandle: SavedStateHandle
) : ViewModel() {

    private val listType: MovieListTypes = requireNotNull(savedStateHandle[MOVIE_LIST_TYPE_ARG_KEY])

    private val favoriteListState: StateFlow<List<Long>> = movieRepository.getFavoriteMoviesIdFlow()
        .stateIn(
            viewModelScope,
            SharingStarted.WhileSubscribed(),
            emptyList()
        )

    private val _moviePagedListState = MutableStateFlow<PagingData<Movie>>(PagingData.empty())
    val moviePagedListState: Flow<PagingData<Movie>> = combine(
        _moviePagedListState.asStateFlow(),
        favoriteListState
    ) { moviePagedListState: PagingData<Movie>, isFavoriteList: List<Long> ->
        moviePagedListState.map { movie ->
            val updatedIsFavorite = isFavoriteList.contains(movie.filmId)
            movie.copy(isFavorite = updatedIsFavorite)
        }
    }.onStart { loadData() }.stateIn(
        viewModelScope,
        SharingStarted.WhileSubscribed(),
        _moviePagedListState.value
    )
        .cachedIn(viewModelScope)

    var query: String = ""

    fun loadData() {
        val pager = movieListUseCase(listType, query)
            .cachedIn(viewModelScope)

        viewModelScope.launch {
            pager.collect {
                _moviePagedListState.value = it
            }
        }
    }

    fun submitQuery(userSearch: String?) {
        query = userSearch ?: ""
        loadData()
    }

    fun toggleFavorites(film: Movie) {
        viewModelScope.launch {
            if (favoriteListState.value.contains(film.filmId)) {
                movieRepository.removeFromFavorites(film)
            } else {
                movieRepository.addToFavorites(film)
            }
        }
    }
}