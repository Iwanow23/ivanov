package ru.ivanov23.cinemalist.ui.movieList

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.appcompat.widget.SearchView
import androidx.core.view.MenuProvider
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.paging.CombinedLoadStates
import androidx.paging.LoadState
import by.kirich1409.viewbindingdelegate.viewBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import ru.ivanov23.cinemalist.R
import ru.ivanov23.cinemalist.data.model.Movie
import ru.ivanov23.cinemalist.data.model.MovieListTypes
import ru.ivanov23.cinemalist.databinding.FragmentMovieListBinding
import ru.ivanov23.cinemalist.databinding.LayoutErrorViewBinding
import ru.ivanov23.cinemalist.ui.movieDetails.MovieDetailsActivity

@AndroidEntryPoint
class MovieListFragment : Fragment(R.layout.fragment_movie_list), MenuProvider {

    private val viewBinding: FragmentMovieListBinding by viewBinding(FragmentMovieListBinding::bind)
    private val viewModel: MovieListViewModel by viewModels()

    private val adapter by lazy {
        MoviePagingAdapter(this::onMovieShortClick, this::onMovieLongClick)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupViews()
        observeDataChanges()
    }

    private fun setupViews() {
        with(viewBinding) {
            moviesListSlider.adapter = adapter
            errorView.setOnInflateListener { _, inflated ->
                LayoutErrorViewBinding.bind(inflated).errorActionButton.setOnClickListener {
                    viewModel.loadData()
                }
            }
        }
        activity?.addMenuProvider(this, viewLifecycleOwner, Lifecycle.State.RESUMED)
        adapter.addLoadStateListener { state: CombinedLoadStates ->
            when(state.refresh) {
                is LoadState.Loading -> showLoadingState()
                is LoadState.Error -> showErrorState()
                is LoadState.NotLoading -> showDataState()
            }
        }
    }

    private fun observeDataChanges() {
        viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
                launch { viewModel.moviePagedListState.collectLatest { data -> adapter.submitData(data) } }
            }
        }
    }

    private fun showLoadingState() {
        with(viewBinding) {
            moviesListSlider.isVisible = false
            emptyListView.isVisible = false
            errorView.isVisible = false
            loaderView.show()
        }
    }

    private fun showErrorState() {
        with(viewBinding) {
            moviesListSlider.isVisible = false
            emptyListView.isVisible = false
            errorView.isVisible = true
            loaderView.hide()
        }
    }

    private fun showDataState() {
        with(viewBinding) {
            moviesListSlider.isVisible = true
            errorView.isVisible = false
            loaderView.hide()

            if (adapter.itemCount == 0) {
                emptyListView.isVisible = true
            }
        }
    }

    override fun onCreateMenu(menu: Menu, menuInflater: MenuInflater) {
        menuInflater.inflate(R.menu.toolbar_menu, menu)

        val searchViewItem = menu.findItem(R.id.action_search)
        val searchView = searchViewItem.actionView as SearchView

        searchView.queryHint = getString(R.string.search_title)

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                viewModel.submitQuery(query)
                viewBinding.moviesListSlider.smoothScrollToPosition(0)
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                viewModel.submitQuery(newText)
                viewBinding.moviesListSlider.smoothScrollToPosition(0)
                return false
            }
        })
    }

    override fun onMenuItemSelected(menuItem: MenuItem): Boolean {
        return false
    }

    private fun onMovieShortClick(movieId: Long) {
        val intent = Intent(requireContext(), MovieDetailsActivity::class.java)
        intent.putExtra(MOVIE_ID_ARG_KEY, movieId)
        startActivity(intent)
    }

    private fun onMovieLongClick(movie: Movie): Boolean {
        viewModel.toggleFavorites(movie)
        return true
    }

    companion object {
        const val MOVIE_ID_ARG_KEY = "MOVIE_ID_KEY"
        const val MOVIE_LIST_TYPE_ARG_KEY = "MOVIE_LIST_TYPE_KEY"

        fun getInstance(listType: MovieListTypes) = MovieListFragment().apply {
            arguments = Bundle().apply {
                putSerializable(MOVIE_LIST_TYPE_ARG_KEY, listType)
            }
        }
    }
}