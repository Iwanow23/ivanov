package ru.ivanov23.cinemalist.ui.mainPage

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.viewpager2.widget.ViewPager2
import by.kirich1409.viewbindingdelegate.viewBinding
import ru.ivanov23.cinemalist.R
import ru.ivanov23.cinemalist.databinding.FragmentMainPageBinding
import ru.ivanov23.cinemalist.utils.setTitle

class MainPageFragment : Fragment(R.layout.fragment_main_page) {

    private val viewBinding: FragmentMainPageBinding by viewBinding(FragmentMainPageBinding::bind)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupViewPager()
        setupButtons()
        setupPageChangeCallback()
    }

    private fun setupViewPager() {
        val pagerAdapter = PagerAdapter(this)
        viewBinding.listViewPager.adapter = pagerAdapter
    }

    private fun setupButtons() {
        viewBinding.popularButton.setOnClickListener {
            viewBinding.listViewPager.currentItem = 0
        }

        viewBinding.favoriteButton.setOnClickListener {
            viewBinding.listViewPager.currentItem = 1
        }
    }

    private fun setupPageChangeCallback() {
        viewBinding.listViewPager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                updatePageTitle(position)
            }
        })
    }

    private fun updatePageTitle(position: Int) {
        val title = when(position) {
            0 -> getString(R.string.popular)
            1 -> getString(R.string.favorite)
            else -> ""
        }
        setTitle(title)
    }
}