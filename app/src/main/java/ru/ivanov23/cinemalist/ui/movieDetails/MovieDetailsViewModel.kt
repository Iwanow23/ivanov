package ru.ivanov23.cinemalist.ui.movieDetails

import android.graphics.Bitmap
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch
import ru.ivanov23.cinemalist.domain.GetMovieDetailsUseCase
import ru.ivanov23.cinemalist.domain.GetMoviePosterUseCase
import ru.ivanov23.cinemalist.data.model.MovieDetails
import ru.ivanov23.cinemalist.data.model.PosterTypes
import ru.ivanov23.cinemalist.ui.movieList.MovieListFragment.Companion.MOVIE_ID_ARG_KEY
import javax.inject.Inject

@HiltViewModel
class MovieDetailsViewModel @Inject constructor(
    private val getMovieDetailsUseCase: GetMovieDetailsUseCase,
    private val getMoviePosterUseCase: GetMoviePosterUseCase,
    savedStateHandle: SavedStateHandle
) : ViewModel() {

    private val filmId: Long = requireNotNull(savedStateHandle[MOVIE_ID_ARG_KEY])

    private val _viewState = MutableStateFlow<MovieDetailsViewState>(MovieDetailsViewState.Loading)
    val viewState: Flow<MovieDetailsViewState> = _viewState
        .onStart { loadData() }
        .stateIn(
            viewModelScope,
            SharingStarted.WhileSubscribed(),
            _viewState.value
        )

    fun loadData() {
        _viewState.value = MovieDetailsViewState.Loading

        val getMovieDetailsDeferred = viewModelScope.async { getMovieDetailsUseCase(filmId) }
        val getMoviePosterDeferred = viewModelScope.async { getMoviePosterUseCase(PosterTypes.FULL, filmId) }

        viewModelScope.launch {
            try {
                val (movieDetails, moviePoster) = listOf(
                    getMovieDetailsDeferred,
                    getMoviePosterDeferred
                ).awaitAll()

                val movieDetailsData = movieDetails as MovieDetails
                val moviePosterBitmap = moviePoster as Bitmap

                val viewData = with(movieDetailsData) {
                    MovieDetails(
                        filmId = filmId,
                        nameRu = nameRu,
                        year = year,
                        genres = genres,
                        description = description,
                        countries = countries,
                        posterUrl = posterUrl,
                        poster = moviePosterBitmap
                    )
                }
                _viewState.value = MovieDetailsViewState.Success(viewData)
            } catch (e: Exception) {
                _viewState.value = MovieDetailsViewState.Error
            }
        }
    }
}