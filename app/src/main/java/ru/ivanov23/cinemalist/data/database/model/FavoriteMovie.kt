package ru.ivanov23.cinemalist.data.database.model

import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Класс данных, представляющий информацию о избранном фильме.
 *
 * @param filmId Уникальный идентификатор фильма.
 * @param nameRu Название фильма на русском языке, может быть null.
 * @param year Год выпуска фильма, может быть null.
 * @param genres Жанры фильма в виде строки, может быть null.
 * @param posterUrl URL постера фильма, может быть null.
 */
@Entity(tableName = "favorite_movies")
data class FavoriteMovie(
    @PrimaryKey val filmId: Long,
    val nameRu: String?,
    val year: String?,
    val genres: String?,
    val posterUrl: String?,
)