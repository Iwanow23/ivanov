package ru.ivanov23.cinemalist.data.model

/**
 * Перечисление, представляющее различные типы постеров.
 */
enum class PosterTypes {
    PREVIEW, // Постер для предпросмотра
    FULL // Полноразмерный постер
}