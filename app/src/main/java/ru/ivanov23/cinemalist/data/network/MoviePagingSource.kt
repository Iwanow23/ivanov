package ru.ivanov23.cinemalist.data.network

import androidx.paging.PagingSource
import androidx.paging.PagingState
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import retrofit2.HttpException
import ru.ivanov23.cinemalist.data.model.Movie
import ru.ivanov23.cinemalist.data.network.model.RestMovie
import java.io.IOException

class MoviePagingSource @AssistedInject constructor(
    private val apisService: MovieApiService,
    @Assisted("query") private val query: String
) : PagingSource<Int, Movie>() {

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Movie> {
        val position = params.key ?: STARTING_PAGE_INDEX
        return try {
            val response =
                if (query.isBlank())
                    apisService.getPopularMovies(position).movies.toMovie()
                else
                    apisService.getMoviesByKeyword(position, query).movies.toMovie()

            val nextKey = if (response.isEmpty()) {
                null
            } else {
                position + (params.loadSize / NETWORK_PAGE_SIZE)
            }
            LoadResult.Page(
                data = response,
                prevKey = if (position == STARTING_PAGE_INDEX) null else position - 1,
                nextKey = nextKey
            )
        } catch (e: IOException) {
            return LoadResult.Error(e)
        } catch (e: HttpException) {
            return LoadResult.Error(e)
        }
    }

    override fun getRefreshKey(state: PagingState<Int, Movie>): Int? {
        val anchorPosition = state.anchorPosition ?: return null
        val anchorPage = state.closestPageToPosition(anchorPosition) ?: return null
        return anchorPage.prevKey?.plus(1) ?: anchorPage.nextKey?.minus(1)
    }

    @AssistedFactory
    interface Factory {
        fun create(@Assisted("query") query: String): MoviePagingSource
    }

    companion object {
        private const val STARTING_PAGE_INDEX = 1
        private const val NETWORK_PAGE_SIZE = 20
    }
}

private fun List<RestMovie>.toMovie(): List<Movie> =
    map { restMovie ->
        Movie(
            filmId = restMovie.filmId,
            nameRu = restMovie.nameRu,
            year = restMovie.year,
            genres = if (restMovie.genres.isNotEmpty()) restMovie.genres[0]["genre"] else "",
            posterUrl = restMovie.posterUrl,
            isFavorite = false
        )
    }