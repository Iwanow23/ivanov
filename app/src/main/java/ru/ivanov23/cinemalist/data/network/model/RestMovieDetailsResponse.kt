package ru.ivanov23.cinemalist.data.network.model

import com.google.gson.annotations.SerializedName

/**
 * Класс данных, представляющий подробную информацию о фильме из REST API.
 *
 * @param filmId Уникальный идентификатор фильма на Кинопоиске.
 * @param nameRu Название фильма на русском языке.
 * @param year Год выпуска фильма.
 * @param genres Список жанров фильма в формате ключ-значение.
 * @param description Описание фильма.
 * @param countries Список стран производства фильма в формате ключ-значение.
 * @param posterUrl URL-адрес постера фильма.
 */
data class RestMovieDetailsResponse(
    @SerializedName("kinopoiskId") val filmId: Long,
    @SerializedName("nameRu") val nameRu: String,
    @SerializedName("year") val year: String,
    @SerializedName("genres") val genres: List<Map<String, String>>,
    @SerializedName("description") val description: String,
    @SerializedName("countries") val countries: List<Map<String, String>>,
    @SerializedName("posterUrl") val posterUrl: String,
)