package ru.ivanov23.cinemalist.data.database

import androidx.paging.PagingSource
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import kotlinx.coroutines.flow.Flow
import ru.ivanov23.cinemalist.data.database.model.FavoriteMovie

/**
 * Dao (Data Access Object) для работы с избранными фильмами в базе данных.
 */
@Dao
interface MovieDao {

    /**
     * Добавить фильм в список избранных.
     *
     * @param movie Фильм для добавления.
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addToFavorites(movie: FavoriteMovie)

    /**
     * Удалить фильм из списка избранных.
     *
     * @param movie Фильм для удаления.
     */
    @Delete
    suspend fun removeFromFavorites(movie: FavoriteMovie)

    /**
     * Получить ID всех избранных фильмов в виде потока.
     *
     * @return Поток с ID избранных фильмов.
     */
    @Query("SELECT filmId FROM favorite_movies")
    fun getFavoriteMoviesIdFlow(): Flow<List<Long>>

    /**
     * Получить избранные фильмы по ключевому слову с поддержкой пагинации.
     *
     * @param query Ключевое слово для поиска фильмов.
     * @return Источник данных для пагинации избранных фильмов.
     */
    @Query("SELECT * FROM favorite_movies WHERE :query = '' OR nameRu LIKE '%' || :query || '%'")
    fun getFavoriteMoviesByKeyword(query: String): PagingSource<Int, FavoriteMovie>
}