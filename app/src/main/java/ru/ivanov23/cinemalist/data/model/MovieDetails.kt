package ru.ivanov23.cinemalist.data.model

import android.graphics.Bitmap

/**
 * Класс данных, представляющий подробную информацию о фильме.
 *
 * @param filmId Уникальный идентификатор фильма.
 * @param nameRu Название фильма на русском языке, может быть null.
 * @param year Год выпуска фильма, может быть null.
 * @param genres Жанры фильма в виде строки, может быть null.
 * @param description Описание фильма, может быть null.
 * @param countries Страны производства фильма в виде строки, может быть null.
 * @param posterUrl URL постера фильма, может быть null.
 * @param poster Изображение постера фильма в формате Bitmap, может быть null.
 */
data class MovieDetails(
    val filmId: Long,
    val nameRu: String?,
    val year: String?,
    val genres: String?,
    val description: String?,
    val countries: String?,
    val posterUrl: String?,
    val poster: Bitmap?
)