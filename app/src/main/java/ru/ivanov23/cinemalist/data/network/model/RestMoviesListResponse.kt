package ru.ivanov23.cinemalist.data.network.model

import com.google.gson.annotations.SerializedName

/**
 * Класс данных, представляющий ответ с списком фильмов из REST API.
 *
 * @param movies Список фильмов в формате списка объектов `RestMovie`.
 */
data class RestMoviesListResponse(
    @SerializedName("films") val movies: List<RestMovie>,
)