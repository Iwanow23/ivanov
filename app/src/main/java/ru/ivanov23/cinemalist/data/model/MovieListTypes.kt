package ru.ivanov23.cinemalist.data.model

/**
 * Перечисление, представляющее различные типы списков фильмов.
 */
enum class MovieListTypes {
    POPULAR, // Популярные фильмы
    FAVORITE // Избранные фильмы
}