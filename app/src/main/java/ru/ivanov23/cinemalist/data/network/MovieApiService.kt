package ru.ivanov23.cinemalist.data.network

import okhttp3.ResponseBody
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import ru.ivanov23.cinemalist.data.network.model.RestMovieDetailsResponse
import ru.ivanov23.cinemalist.data.network.model.RestMoviesListResponse

/**
 * Интерфейс для взаимодействия с API кинопоиска.
 */
interface MovieApiService {

    /**
     * Получить список популярных фильмов.
     *
     * @param page Номер страницы.
     * @param type Тип запроса (по умолчанию "TOP_100_POPULAR_FILMS").
     * @return Ответ с списком популярных фильмов.
     */
    @GET("/api/v2.2/films/top")
    suspend fun getPopularMovies(
        @Query("page") page: Int,
        @Query("type") type: String = "TOP_100_POPULAR_FILMS"
    ): RestMoviesListResponse

    /**
     * Получить фильмы по ключевому слову.
     *
     * @param page Номер страницы.
     * @param keyword Ключевое слово для поиска фильмов.
     * @return Ответ с найденными фильмами.
     */
    @GET("/api/v2.1/films/search-by-keyword")
    suspend fun getMoviesByKeyword(
        @Query("page") page: Int,
        @Query("keyword") keyword: String,
    ): RestMoviesListResponse

    /**
     * Получить детали о фильме по его ID.
     *
     * @param filmId ID фильма.
     * @return Детали выбранного фильма.
     */
    @GET("/api/v2.2/films/{filmId}")
    suspend fun getMovieDetails(@Path("filmId") filmId: Long): RestMovieDetailsResponse

    /**
     * Получить миниатюру постера фильма.
     *
     * @param filmId ID фильма.
     * @return Байтовое тело миниатюры постера.
     */
    @GET("/images/posters/kp_small/{filmId}.jpg")
    suspend fun getMoviePosterPreview(@Path("filmId") filmId: Long): ResponseBody

    /**
     * Получить постер фильма.
     *
     * @param filmId ID фильма.
     * @return Байтовое тело постера фильма.
     */
    @GET("/images/posters/kp/{filmId}.jpg")
    suspend fun getMoviePoster(@Path("filmId") filmId: Long): ResponseBody
}