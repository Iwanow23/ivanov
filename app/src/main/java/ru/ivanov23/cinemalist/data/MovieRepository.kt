package ru.ivanov23.cinemalist.data

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.map
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import ru.ivanov23.cinemalist.data.database.MovieDao
import ru.ivanov23.cinemalist.data.database.model.FavoriteMovie
import ru.ivanov23.cinemalist.data.network.MoviePagingSource
import ru.ivanov23.cinemalist.data.model.Movie
import javax.inject.Inject

class MovieRepository @Inject constructor(
    private val moviePagingSource: MoviePagingSource.Factory,
    private val movieDao: MovieDao
) {

    fun getMovieListFromApi(query: String): Flow<PagingData<Movie>> =
        Pager(
            PagingConfig(
                pageSize = 20,
                enablePlaceholders = false
            ),
            pagingSourceFactory = { moviePagingSource.create(query) }
        )
            .flow

    fun getMovieListFromDb(query: String): Flow<PagingData<Movie>> =
        Pager(
            PagingConfig(
                pageSize = 20,
                enablePlaceholders = false
            ),
            pagingSourceFactory = { movieDao.getFavoriteMoviesByKeyword(query) }
        )
            .flow.map { pagingData ->
                pagingData.map { movie ->
                    movie.toMovie()
                }
            }

    fun getFavoriteMoviesIdFlow(): Flow<List<Long>> =
        movieDao.getFavoriteMoviesIdFlow()

    suspend fun addToFavorites(movie: Movie) {
        movieDao.addToFavorites(movie.toFavoriteMovie())
    }

    suspend fun removeFromFavorites(movie: Movie) {
        movieDao.removeFromFavorites(movie.toFavoriteMovie())
    }
}


private fun Movie.toFavoriteMovie(): FavoriteMovie =
    FavoriteMovie(filmId, nameRu, year, genres, posterUrl)

private fun FavoriteMovie.toMovie(): Movie =
    Movie(filmId, nameRu, year, genres, posterUrl, true)