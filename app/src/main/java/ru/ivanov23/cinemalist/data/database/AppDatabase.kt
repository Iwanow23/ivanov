package ru.ivanov23.cinemalist.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import ru.ivanov23.cinemalist.data.database.model.FavoriteMovie

@Database(
    entities = [FavoriteMovie::class],
    exportSchema = false,
    version = 1,
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun favoriteMovieDao(): MovieDao
}