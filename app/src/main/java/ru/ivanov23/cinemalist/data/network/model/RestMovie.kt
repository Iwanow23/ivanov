package ru.ivanov23.cinemalist.data.network.model

import com.google.gson.annotations.SerializedName

/**
 * Класс данных, представляющий информацию о фильме из REST API.
 *
 * @param filmId Уникальный идентификатор фильма.
 * @param nameRu Название фильма на русском языке.
 * @param year Год выпуска фильма.
 * @param genres Список жанров фильма в формате ключ-значение.
 * @param posterUrl URL-адрес превью постера фильма.
 */
data class RestMovie(
    @SerializedName("filmId") val filmId: Long,
    @SerializedName("nameRu") val nameRu: String,
    @SerializedName("year") val year: String,
    @SerializedName("genres") val genres: List<Map<String, String>>,
    @SerializedName("posterUrlPreview") val posterUrl: String,
)