package ru.ivanov23.cinemalist.data.model

/**
 * Класс данных, представляющий информацию о фильме.
 *
 * @param filmId Уникальный идентификатор фильма.
 * @param nameRu Название фильма на русском языке, может быть null.
 * @param year Год выпуска фильма, может быть null.
 * @param genres Жанры фильма в виде строки, может быть null.
 * @param posterUrl URL постера фильма, может быть null.
 * @param isFavorite Флаг, указывающий, является ли фильм избранным.
 */
data class Movie(
    val filmId: Long,
    val nameRu: String?,
    val year: String?,
    val genres: String?,
    val posterUrl: String?,
    var isFavorite: Boolean
)